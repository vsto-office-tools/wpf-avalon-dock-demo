﻿using AvalonDock.Layout;
using System.Windows;

namespace avalonDockDemo.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        public int anchorableIndex { get; set; } = 0;
        public int documentIndex { get; set; } = 0;
        private void Button_Click_AnchorablePane(object sender, RoutedEventArgs e)
        {
            if (anchorableIndex >= 5)
            {
                MessageBox.Show("添加窗格数量已达到上限");
                return;
            }
            LayoutAnchorable anchorable = new LayoutAnchorable();
            anchorable.Title = "窗格" + anchorableIndex;
            anchorableIndex += 1;
            anchorable.CanFloat = true;
            anchorable.CanAutoHide = false;
            anchorable.CanHide = false;
            AnchorablePane.Children.Add(anchorable);
        }

        private void Button_Click_DocumentPane(object sender, RoutedEventArgs e)
        {
            if (documentIndex >= 5)
            {
                MessageBox.Show("添加文档数量已达到上限");
                return;
            }
            LayoutDocument document = new LayoutDocument();
            document.Title = "文档" + documentIndex;
            documentIndex += 1;
            DocumentPane.Children.Add(document);
        }
    }
}
