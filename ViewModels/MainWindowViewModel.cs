﻿using AvalonDock.Layout;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Legends;
using OxyPlot.Series;
using OxyPlot.Wpf;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace avalonDockDemo.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "AvalondDock Dome";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }


        public DelegateCommand AddCommand { get; private set; }
        private PlotModel plotView;
        private PlotModel myPlotView;
        private bool isTaskRunning;

        public PlotModel PlotView
        {
            get { return plotView; }
            set { plotView = value; }
        }

        public LineSeries mySeries { get; private set; }

        private void InitPlotView()
        {
            myPlotView = new PlotModel();
            var linearAxis = new LinearAxis();
            linearAxis.MajorGridlineStyle = LineStyle.Solid;
            linearAxis.MinorGridlineStyle = LineStyle.Dot;
            myPlotView.Axes.Add(linearAxis);
            mySeries = new LineSeries { Title = "series", Color = OxyColors.Green };
            myPlotView.Series.Add(mySeries);
            mySeries.Points.Clear();

            PlotView = myPlotView;
        }
        private void InitBackgroundWorker()
        {
            Worker = new BackgroundWorker();
            Worker.DoWork += Worker_DoWork;
            Worker.ProgressChanged += Worker_ProgressChanged;
            Worker.WorkerReportsProgress = true;
        }
        private static double frequency = 1;
        public BackgroundWorker Worker { get; set; }
        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double amplitude = 1;
            double phaseShift = 0;
            double xMin = 0;
            double xMax = 100;
            double stepSize = 0.1;
            mySeries.Points.Clear();
            for (double t = 0; t <= xMax - xMin; t += stepSize)
            {
                double x = xMin + t;
                double y = amplitude * Math.Sin(2 * Math.PI * frequency * x + phaseShift);
                mySeries.Points.Add(new DataPoint(x, y));
            }
            frequency += 0.1;
            if (frequency >= 2)
            {
                frequency = 1;
            }
             myPlotView.InvalidatePlot(true);
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker background = sender as BackgroundWorker;
            while (true)
            {
                background.ReportProgress(0);
                Thread.Sleep(1000);
            }
        }

        public MainWindowViewModel()
        {
            AddCommand = new DelegateCommand(Add);
            InitPlotView();
            InitBackgroundWorker();
        }
        
        private void Add()
        {
            if (!Worker.IsBusy)
            {
                Worker.RunWorkerAsync();
            }
        }
    }
}
